package ru.tsc.ichaplygina.taskmanager.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.listener.LoggerListener;
import ru.tsc.ichaplygina.taskmanager.service.ReceiverService;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    ReceiverService receiverService;


    @SneakyThrows
    public void init() {
        receiverService.receive(new LoggerListener());
    }

}
