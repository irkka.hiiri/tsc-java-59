package ru.tsc.ichaplygina.taskmanager.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.ISessionService;
import ru.tsc.ichaplygina.taskmanager.endpoint.AdminEndpoint;
import ru.tsc.ichaplygina.taskmanager.endpoint.SessionEndpoint;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Getter
@Setter
@Component
public abstract class AbstractListener {

    @NotNull
    protected static final String DESCRIPTION_INPUT = "Please enter description: ";

    @NotNull
    protected static final String ID_INPUT = "Please enter id: ";

    @NotNull
    protected static final String INDEX_INPUT = "Please enter index: ";

    @NotNull
    protected static final String NAME_INPUT = "Please enter name: ";

    @NotNull
    @Autowired
    protected AdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected ISessionService sessionService;

    @Nullable
    public abstract String argument();

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void handler(@NotNull final ConsoleEvent consoleEvent);

    @Override
    public String toString() {
        return (command() + (isEmptyString(argument()) ? "" : " [" + argument() + "]") + " - " + description() + ".");
    }

}
