package ru.tsc.ichaplygina.taskmanager.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class FileScanner implements Runnable {

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public void init() {
        start();
    }

    @Override
    @SneakyThrows
    public void run() {
        @NotNull final String path = propertyService.getFileScannerPath();
        @NotNull final List<String> files = Files.list(Paths.get(path))
                .filter(file -> !Files.isDirectory(file))
                .map(Path::getFileName)
                .map(Path::toString)
                .collect(Collectors.toList());
        for (final String fileName : files) {
            for (@NotNull final AbstractListener listener : listeners) {
                if (fileName.equals(listener.command())) {
                    publisher.publishEvent(new ConsoleEvent(fileName));
                    System.out.println(fileName);
                    Files.delete(Paths.get(fileName));
                }
            }
        }
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this, 0, propertyService.getFileScannerFrequency(), TimeUnit.MILLISECONDS);
    }
}
