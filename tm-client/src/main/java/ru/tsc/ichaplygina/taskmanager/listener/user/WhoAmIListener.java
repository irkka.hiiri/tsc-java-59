package ru.tsc.ichaplygina.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.endpoint.Session;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

@Component
public final class WhoAmIListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "print current user login";
    @NotNull
    public static final String NAME = "whoami";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@whoAmIListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        final Session currentSession = sessionService.getSession();
        if (currentSession == null) System.out.println("User not logged in");
        else
            System.out.println(getAdminEndpoint().findUserById(currentSession, currentSession.getUser().getId()).getLogin());
    }

}
