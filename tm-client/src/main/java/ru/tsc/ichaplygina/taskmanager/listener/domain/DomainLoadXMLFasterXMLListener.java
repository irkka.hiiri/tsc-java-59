package ru.tsc.ichaplygina.taskmanager.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

@Component
public class DomainLoadXMLFasterXMLListener extends AbstractDomainListener {

    @NotNull
    public final static String DESCRIPTION = "load projects, tasks and users from xml file using fasterxml";
    @NotNull
    public final static String NAME = "load xml fasterxml";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@domainLoadXMLFasterXMLListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        getAdminEndpoint().loadXMLFasterXML(sessionService.getSession());
    }

}
