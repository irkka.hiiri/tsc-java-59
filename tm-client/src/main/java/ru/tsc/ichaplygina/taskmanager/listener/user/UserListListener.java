package ru.tsc.ichaplygina.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.endpoint.User;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.DELIMITER;

@Component
public final class UserListListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "show all users";
    @NotNull
    public static final String NAME = "list users";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userListListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        final List<User> userList = getAdminEndpoint().getUsers(sessionService.getSession());
        System.out.println("Id : Login : Role : E-mail : First Name : Middle Name : Last Name : Locked");
        for (final User user : userList) {
            System.out.println(user.getId() + DELIMITER + user.getLogin() + DELIMITER + user.getRole() + DELIMITER +
                    user.getEmail() + DELIMITER + user.getFirstName() + DELIMITER + user.getMiddleName() + DELIMITER +
                    user.getLastName() + DELIMITER + user.isLocked());
        }
    }

}
