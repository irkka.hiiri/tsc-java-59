package ru.tsc.ichaplygina.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class UserAddListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "add a new user";
    @NotNull
    public static final String NAME = "add user";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userAddListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String password = readLine(ENTER_PASSWORD);
        @NotNull final String email = readLine("Enter email: ");
        @NotNull final String role = readLine(ENTER_ROLE);
        @NotNull final String firstName = readLine("Enter first name: ");
        @NotNull final String middleName = readLine("Enter middle name: ");
        @NotNull final String lastName = readLine("Enter last name: ");
        getAdminEndpoint().addUser(sessionService.getSession(), login, password, email, role, firstName, middleName, lastName);
    }

}
