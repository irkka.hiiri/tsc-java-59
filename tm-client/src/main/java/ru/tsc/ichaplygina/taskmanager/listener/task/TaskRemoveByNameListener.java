package ru.tsc.ichaplygina.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class TaskRemoveByNameListener extends AbstractTaskListener {

    @NotNull
    public final static String DESCRIPTION = "remove task by name";
    @NotNull
    public final static String NAME = "remove task by name";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskRemoveByNameListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String name = readLine(NAME_INPUT);
        getTaskEndpoint().removeTaskByName(sessionService.getSession(), name);
    }

}
