package ru.tsc.ichaplygina.taskmanager.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.property.IApplicationProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.INetworkProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.IPasswordProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.ISessionProperty;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService, IPasswordProperty, IApplicationProperty, ISessionProperty, INetworkProperty {

    @Value("#{environment['application.version']}")
    private String applicationVersion;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['autosave.frequency']}")
    private Integer autosaveFrequency;

    @Value("#{environment['autosave.onExit']}")
    private Boolean autosaveOnExit;

    @Value("#{environment['fileScanner.frequency']}")
    private Integer fileScannerFrequency;

    @Value("#{environment['fileScanner.path']}")
    private String fileScannerPath;

    @Value("#{environment['session.sign.iteration']}")
    private Integer signIteration;

    @Value("#{environment['session.sign.secret']}")
    private String signSecret;

    @Value("#{environment['application.port']}")
    private String port;

    @Value("#{environment['application.server']}")
    private String server;

}
