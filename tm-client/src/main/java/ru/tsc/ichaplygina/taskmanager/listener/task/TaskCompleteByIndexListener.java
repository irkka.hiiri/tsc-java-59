package ru.tsc.ichaplygina.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

@Component
public final class TaskCompleteByIndexListener extends AbstractTaskListener {

    @NotNull
    public final static String DESCRIPTION = "complete task by index";
    @NotNull
    public final static String NAME = "complete task by index";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCompleteByIndexListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        final int index = readNumber(INDEX_INPUT);
        getTaskEndpoint().completeTaskByIndex(sessionService.getSession(), index);
    }

}
