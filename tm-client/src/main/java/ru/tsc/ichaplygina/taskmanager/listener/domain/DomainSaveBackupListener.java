package ru.tsc.ichaplygina.taskmanager.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

@Component
public class DomainSaveBackupListener extends AbstractDomainListener {

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to backup xml file " +
            "which is loaded on application start. Runs automatically and can be run manually.";
    @NotNull
    public final static String NAME = "save backup";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }


    @Override
    @SneakyThrows
    @EventListener(condition = "@domainSaveBackupListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        getAdminEndpoint().saveBackup(sessionService.getSession());
    }

}
