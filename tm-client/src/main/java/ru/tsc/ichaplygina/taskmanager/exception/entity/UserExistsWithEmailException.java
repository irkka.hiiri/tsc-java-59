package ru.tsc.ichaplygina.taskmanager.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class UserExistsWithEmailException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User already exists with email: ";

    public UserExistsWithEmailException(final String email) {
        super(MESSAGE + email);
    }

}
