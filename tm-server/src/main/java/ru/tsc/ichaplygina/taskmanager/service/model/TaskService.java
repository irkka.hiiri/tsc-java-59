package ru.tsc.ichaplygina.taskmanager.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.ITaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskService extends AbstractBusinessEntityService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final Task task) {
        repository.add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final User user = Optional.ofNullable(userService.findById(userId)).orElseThrow(UserNotFoundException::new);
        @NotNull final Task task = new Task(name, description, user);
        add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Task> taskList) {
        if (taskList == null) return;
        for (final Task task : taskList) add(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public final Task addTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @Nullable final Task task = userService.isPrivilegedUser(userId) ?
                repository.findById(taskId) : repository.findByIdForUser(userId, taskId);
        if (task == null) return null;
        @Nullable final Project project = Optional.ofNullable(projectService.findById(userId, projectId)).orElseThrow(ProjectNotFoundException::new);
        task.setProject(project);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(final String userId) {
        if (userService.isPrivilegedUser(userId)) repository.clear();
        else repository.clearForUser(userId);
    }

    @Nullable
    @Override
    @Transactional
    public Task completeById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.COMPLETED);
    }

    @Nullable
    @Override
    @Transactional
    public Task completeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByIndex(index) :
                repository.getIdByIndexForUser(userId, index);
        return completeById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public Task completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        if (id == null) return null;
        return completeById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.findAll() : repository.findAllForUser(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<Task> comparator = getComparator(sortBy);
        return userService.isPrivilegedUser(userId) ?
                repository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                repository.findAllForUser(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public final List<Task> findAllByProjectId(@NotNull final String userId,
                                               @NotNull final String projectId,
                                               @Nullable final String sortBy) {
        @NotNull final Comparator<Task> comparator = getComparator(sortBy);
        return userService.isPrivilegedUser(userId) ?
                repository.findAllByProjectId(projectId).stream().sorted(comparator).collect(Collectors.toList()) :
                repository.findAllByProjectIdForUser(userId, projectId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findById(taskId) : repository.findByIdForUser(userId, taskId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        return userService.isPrivilegedUser(userId) ?
                repository.findByIndex(entityIndex) : repository.findByIndexForUser(userId, entityIndex);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findByName(entityName) : repository.findByNameForUser(userId, entityName);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByIndex(index) : repository.getIdByIndexForUser(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByName(entityName) : repository.getIdByNameForUser(userId, entityName);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.getSize();
    }

    @Override
    @SneakyThrows
    public long getSize(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.getSize() : repository.getSizeForUser(userId);
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Override
    @SneakyThrows
    @Transactional
    public final void removeAllByProjectId(@NotNull final String projectId) {
        repository.removeAllByProjectId(projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final Task task = findById(id);
        if (task == null) return null;
        repository.removeById(id);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.removeById(id);
        else repository.removeByIdForUser(userId, id);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.removeByIndex(index);
        else repository.removeByIndexForUser(userId, index);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.removeByName(name);
        else repository.removeByNameForUser(userId, name);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public final Task removeTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @Nullable final Task task = userService.isPrivilegedUser(userId) ?
                repository.findTaskInProject(taskId, projectId) : repository.findTaskInProjectForUser(userId, taskId, projectId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setProject(null);
        repository.update(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task startById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    @Transactional
    public Task startByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByIndex(index) :
                repository.getIdByIndexForUser(userId, index);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task updateById(@NotNull final String userId,
                           @NotNull final String taskId,
                           @NotNull final String taskName,
                           @Nullable final String taskDescription) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        if (isEmptyString(taskName)) throw new NameEmptyException();
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setName(taskName);
        task.setDescription(taskDescription);
        repository.update(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task updateByIndex(@NotNull final String userId,
                              final int entityIndex,
                              @NotNull final String entityName,
                              @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    @Transactional
    private Task updateStatus(@NotNull final String userId, @Nullable final String taskId, @NotNull final Status status) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setStatus(status);
        repository.update(task);
        return task;
    }

}
