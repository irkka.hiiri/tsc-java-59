package ru.tsc.ichaplygina.taskmanager.repository.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.ISessionRecordRepository;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class SessionRecordRepository extends AbstractRecordRepository<SessionDTO> implements ISessionRecordRepository {

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM SessionDTO e").executeUpdate();
    }

    @Override
    public @NotNull List<SessionDTO> findAll() {
        return entityManager.createQuery("FROM SessionDTO", SessionDTO.class).getResultList();
    }

    @Override
    public @Nullable SessionDTO findById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(1) FROM SessionDTO e", Long.class).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(SessionDTO.class, id));
    }
}
