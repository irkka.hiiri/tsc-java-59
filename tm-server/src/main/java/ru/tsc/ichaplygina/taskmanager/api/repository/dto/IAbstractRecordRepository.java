package ru.tsc.ichaplygina.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

public interface IAbstractRecordRepository<E extends AbstractModelDTO> {

    void add(@NotNull E entity);

    void clear();

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    long getSize();

    @NotNull
    EntityManager getEntityManager();

    void removeById(@NotNull String id);

    void update(E entity);
}
