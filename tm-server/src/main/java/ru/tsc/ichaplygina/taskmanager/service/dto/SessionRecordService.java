package ru.tsc.ichaplygina.taskmanager.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.ISessionRecordRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.ISessionRecordService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IUserRecordService;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.SessionNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedException;

import java.util.List;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.SignatureUtil.sign;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionRecordService extends AbstractRecordService<SessionDTO> implements ISessionRecordService {

    @NotNull
    @Autowired
    private ISessionRecordRepository repository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserRecordService userService;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final SessionDTO session) {
        repository.add(session);
    }

    @Override
    @Transactional
    public void addAll(@Nullable List<SessionDTO> sessionList) {
        if (sessionList == null) return;
        for (final SessionDTO session : sessionList) add(session);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    @Transactional
    public final void closeSession(@NotNull final SessionDTO session) {
        removeById(session.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.getSize();
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public final SessionDTO openSession(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final UserDTO user = Optional.ofNullable(userService.findByLoginForAuthorization(login)).orElseThrow(IncorrectCredentialsException::new);
        if (!user.getPasswordHash().equals(salt(password, propertyService)))
            throw new IncorrectCredentialsException();
        if (user.isLocked()) throw new AccessDeniedException();
        @NotNull final SessionDTO session = new SessionDTO(user.getId());
        sign(session, propertyService);
        repository.add(session);
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SessionDTO session = Optional.ofNullable(repository.findById(id)).orElseThrow(SessionNotFoundException::new);
        repository.removeById(id);
        return session;
    }

    @Override
    @SneakyThrows
    public final void validatePrivileges(@NotNull final String userId) {
        if (!userService.isPrivilegedUser(userId))
            throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public final void validateSession(@Nullable final SessionDTO session) {
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        @Nullable final String signature = session.getSignature();
        Optional.ofNullable(signature).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(findById(session.getId())).orElseThrow(AccessDeniedException::new);
        @NotNull final SessionDTO sessionClone = session.clone();
        if (!signature.equals(sign(sessionClone, propertyService).getSignature()))
            throw new AccessDeniedException();
    }

}
