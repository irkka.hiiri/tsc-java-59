package ru.tsc.ichaplygina.taskmanager.exception.other;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class ServiceLocatorNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Service locator not found!";

    public ServiceLocatorNotFoundException() {
        super(MESSAGE);
    }

}
