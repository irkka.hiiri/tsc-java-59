package ru.tsc.ichaplygina.taskmanager.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['application.version']}")
    private String applicationVersion;

    @Value("#{environment['autosave.frequency']}")
    private Integer autosaveFrequency;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @Value("#{environment['database.sql_dialect']}")
    private String databaseSqlDialect;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("#{environment['database.username']}")
    private String databaseUsername;

    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLvlCache;

    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseCacheConfigFile;

    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheFactoryClass;

    @Value("#{environment['fileScanner.frequency']}")
    private Integer fileScannerFrequency;

    @Value("#{environment['fileScanner.path']}")
    private String fileScannerPath;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['application.port']}")
    private String port;

    @Value("#{environment['application.server']}")
    private String server;

    @Value("#{environment['session.sign.iteration']}")
    private Integer signIteration;

    @Value("#{environment['session.sign.secret']}")
    private String signSecret;

    @Value("#{environment['autosave.onExit']}")
    private Boolean autosaveOnExit;

}
