package ru.tsc.ichaplygina.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.listener.EntityListener;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import static java.lang.System.currentTimeMillis;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_session")
@XmlAccessorType(XmlAccessType.FIELD)
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session extends AbstractModel implements Cloneable {

    @Nullable
    @Column
    private String signature;

    @Column(name = "time_stamp")
    private long timeStamp = currentTimeMillis();

    @NotNull
    @ManyToOne
    @JsonIgnore
    private User user;

    public Session(@NotNull User user) {
        this.user = user;
    }

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }

    @Override
    public String toString() {
        return getId() + " " + user.getId() + "" + timeStamp;
    }

}
